fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
      const list = document.createElement('ul');
      list.className = 'films__list';
      
      const sortedByEpisodes = [...data].sort((a, b) => a.episodeId - b.episodeId);

      sortedByEpisodes.forEach(({episodeId, name, openingCrawl, characters}) => {
        new Film(episodeId, name, openingCrawl, characters).render(list);
      });

      document.querySelector('.container').append(list);
    });

class Film {
  constructor(episodeId, name, openingCrawl, characters) {
    this.episodeId = episodeId;
    this.name = name;
    this.openingCrawl = openingCrawl;
    this.characters = characters;
    this.item = document.createElement('li');
    this.list = null;
  }

  createListItem() {
    this.getCharacters();
    this.item.className = 'films__item';

    const filmName = document.createElement('p'),
          filmDescr = document.createElement('p');

    filmName.innerHTML = `
      <span class='films__id'>${this.episodeId}.</span>
      <span class='films__name'>${this.name}</span>
    `;
    filmDescr.className = 'films__descr';
    filmDescr.innerText = this.openingCrawl;
    console.log(this.list);
    this.item.append(filmName, this.list, filmDescr);
  }

  getCharacters() {
    const list = document.createElement('ul');
    list.classList.add('films__characters');
    list.innerHTML = '<div class="loader"></div>'

    const arr = [];

    this.characters.forEach(url => {
      fetch(url)
        .then(response => response.json())
        .then(({name}) => {
          arr.push(name);

          if (arr.length == this.characters.length) {
            list.innerHTML = '';
            
            arr.forEach((item, i) => {
              const li = document.createElement('li');
              li.innerText = `${i + 1} - ${item}`;
              list.append(li);
            });
          }
        });
    });
    
    this.list = list;
  }

  render(parentElem) {
    this.createListItem();
    parentElem.append(this.item);
  }
}